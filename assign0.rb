def sum(nums)
  total = 0
  nums.each {|x| total += x}
  return total
end

def max_2_sum(nums)
  sum = 0
  a = nums.sort {|x,y| y <=> x }
  sum += a[0] if a[0]
  sum += a[1] if a[1]
  return sum
end

def sum_to_n?(nums, n)
  return false if nums.length < 2
  
  0.upto(nums.length - 1) { |x| 
    (x+1).upto(nums.length - 1) { |y| return true if n == nums[x] + nums[y] }
  }
 
  return false
end

