class BookInStock
  attr_accessor :isbn, :price
  def initialize(isbn, price)
    raise ArgumentError if isbn == "" || price <= 0
    @isbn = isbn
    @price = price
  end
  
  def price_as_string
    if @price.to_s =~ /\.[0-9]{2}\z/
      return "$#{@price}"
    end
    
    if @price.to_s =~ /\.[0-9]{1}\z/
      return "$#{@price}0"
    end
    
    if @price.to_s =~ /[0-9]\z/
        return "$#{@price}.00"
    end
  end
end