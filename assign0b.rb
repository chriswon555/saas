def hello(name)
  "Hello, #{name}"
end

def starts_with_consonant?(s)
  return false if s[0] == nil
  x = s[0].upcase
  
  return true if x =~ /[a-zA-Z]/ && !(x == "A" || x == "E" || x == "I" || x == "O" || x == "U")
  return false
end

def binary_multiple_of_4?(s)
  #s.each_char {|x| return false if !(x =~ /[0-1]/)}
  s.each_char {|x| return false if !(x == "0" || x == "1")}
  return true if s =~ /100\z/
  return true if s == "0"
  return false
end
